ARG ALPINE_VERSION=3.10.3
FROM alpine:${ALPINE_VERSION}

# Prerequisites
RUN apk add curl

ARG ARDUINO_CLI_VERSION=0.6.0
RUN mkdir /tmp/arduino \
  && curl -fsSL -o /tmp/arduino/arduino-cli.tgz \
    https://github.com/arduino/arduino-cli/releases/download/${ARDUINO_CLI_VERSION}/arduino-cli_${ARDUINO_CLI_VERSION}_Linux_64bit.tar.gz \
  && tar -xvf /tmp/arduino/arduino-cli.tgz -C /tmp/arduino/ \
  && mv /tmp/arduino/arduino-cli /usr/local/bin \
  && chmod 755 /usr/local/bin/arduino-cli

RUN arduino-cli core update-index \
  && arduino-cli core download arduino:avr \
  && arduino-cli core install arduino:avr

# Image args, defined at build time.
ARG BUILD_DATE
ARG NAME
ARG VCS_COMMIT
ARG VCS_URL

LABEL \
  ca.paullalonde.arduino-cli-version="${ARDUINO_CLI_VERSION}" \
  org.label-schema.build-date="${BUILD_DATE}" \
  org.label-schema.name="${NAME}" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.vcs-ref="${VCS_COMMIT}" \
  org.label-schema.vcs-url="${VCS_URL}" \
  org.label-schema.vendor="paullalonde"
