#!/bin/bash

set -eux

DOCKER_REGISTRY=paullalonde
DOCKER_REPOSITORY=arduino-cli
DOCKER_TAG=`date '+%Y-%m-%d'`
DOCKER_IMAGE="${DOCKER_REGISTRY}/${DOCKER_REPOSITORY}:${DOCKER_TAG}"

BUILD_DATE=`date -u '+%Y-%m-%dT%H:%M:%SZ'`
GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`
GIT_COMMIT=`git rev-parse HEAD`
GIT_URL=`git config --get remote.origin.url`

docker build \
  -t "${DOCKER_IMAGE}" \
  --build-arg "NAME=${DOCKER_REPOSITORY}" \
  --build-arg "BUILD_DATE=${BUILD_DATE}" \
  --build-arg "VCS_COMMIT=${GIT_COMMIT}" \
  --build-arg "VCS_URL=${GIT_URL}" \
  .

docker push "${DOCKER_IMAGE}"
