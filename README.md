# arduino-cli

Constructs a Docker image containing the Arduino CLI.

The built image is available on [Docker Hub](https://cloud.docker.com/repository/docker/paullalonde/aws-cloud-deploy-kit).
